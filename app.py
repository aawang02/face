from flask import Flask, render_template, request, jsonify
import cv2
import numpy as np
from keras.models import load_model

facedetect = cv2.CascadeClassifier('haarcascade_frontalface_default.xml')
model = load_model('keras_model.h5')

app = Flask(__name__)

@app.route('/')
def home():
    return render_template('home.html')


@app.route('/database_builder')
def database_builder():
    return render_template('database_builder.html')


@app.route('/tester')
def tester():
    return render_template('tester.html')


@app.route('/about')
def about():
    return render_template('about.html')

@app.route('/contact')
def contact():
    return render_template('contact.html')

@app.route('/process_frame', methods=['POST'])
def process_frame():
    file = request.files['frame'].read()
    npimg = np.fromstring(file, np.uint8)
    frame = cv2.imdecode(npimg, cv2.IMREAD_COLOR)
    # detect faces
    faces = facedetect.detectMultiScale(frame, 1.3, 5)
    # get class_idx of each face
    response_data = []
    for (x, y, w, h) in faces:
        crop_face = frame[y:y+h, x:x+w]
        crop_face = cv2.resize(crop_face, (224, 224))
        crop_face = crop_face.reshape(1, 224, 224, 3)
        predict = model.predict(crop_face)
        
        class_idx = np.argmax(predict, axis=1)[0]
        probability = np.amax(predict)

        response_data.append({
            'x': int(x),
            'y': int(y),
            'w': int(w),
            'h': int(h),
            'class_idx': str(class_idx),
            'probability': str(probability)
        })
    return jsonify(response_data)

if __name__ == '__main__':
    app.run(debug=True)
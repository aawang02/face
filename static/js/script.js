// script.js
document.addEventListener('DOMContentLoaded', function() {
    const video = document.getElementById('webcam');
    const startButton = document.getElementById('startButton');
    const stopButton = document.getElementById('stopButton');
    const canvas = document.getElementById('canvas');
    const ctx = canvas.getContext('2d');

    let stream = null;
    let captureInterval = null;

    startButton.addEventListener('click', function() {
        if (navigator.mediaDevices.getUserMedia) {
            navigator.mediaDevices.getUserMedia({ video: true })
                .then(function (strm) {
                    stream = strm;
                    video.srcObject = stream;
                    captureInterval = setInterval(captureFrame, 100);
                })
                .catch(function (error) {
                    console.log("Something went wrong!");
                });
        }
    });

    stopButton.addEventListener('click', function() {
        console.log("Stop button clicked");
        if (stream) {
            console.log("Stopping stream");
            const tracks = stream.getTracks();
            tracks.forEach(track => {
                console.log("Stopping track: ", track);
                track.stop();
            });
            video.srcObject = null;
            stream = null;
            clearInterval(captureInterval);
            ctx.clearRect(0, 0, canvas.width, canvas.height);
        } else {
            console.log("No stream to stop");
        }
    });

    function captureFrame() {
        if(!stream) {
            console.log("No stream to capture");
            return;
        }

        const canvas = document.createElement('canvas');
        canvas.width = video.videoWidth;
        canvas.height = video.videoHeight;
        canvas.getContext('2d').drawImage(video, 0, 0);
        canvas.toBlob(sendFrameToServer, 'image/jpeg');
    }

    function sendFrameToServer(blob) {
        const formData = new FormData();
        formData.append('frame', blob);

        fetch('/process_frame', {
            method: 'POST',
            body: formData
        })
        .then(response => response.json())
        .then(data => {
            console.log('Success:', data);
            // clear previous drawings
            ctx.clearRect(0, 0, canvas.width, canvas.height);
            // draw new bounding boxes
            data.forEach(box => {
                ctx.beginPath();
                ctx.rect(box.x, box.y, box.w, box.h);
                ctx.lineWidth = 2;
                ctx.strokeStyle = 'green';
                ctx.stroke();

                ctx.fillStyle = 'red';
                ctx.fillText(`Class: ${box.class_idx}, Prob: ${box.probability}`, box.x, box.y - 10);
            });
        })
        .catch(error => {
            console.error('Error:', error);
        });
    }
});
